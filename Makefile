.PHONY: %

typesetter_dir=src

%:
	cd "$(typesetter_dir)" && $(MAKE) $@
