# What are the common names for stages of an application development iteration?

Referring to GitLab as an authoritity, the names for these stages are:

1. Build,
2. Test,
3. Deploy.

This conforms with my experience, and does not add anything surprising to it.

# How to execute a build-test-deploy pipeline locally from a GitLab yaml file?

Referring to [Akita on Rails](http://www.akitaonrails.com/2018/04/28/smalltips-running-gitlab-ci-runner-locally):

1. Install GitLab Runner locally,
2. Specify the YML file,
3. Run code.

To install GitLab Runner, go to [installation instructions](https://docs.gitlab.com/runner/install/linux-repository.html).


