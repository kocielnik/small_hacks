#!/usr/bin/env bats

source get_integrity --source-only

@test "get_integrity" {
  expected="OLBgp1GsljhM2TJ+sbHjaiH9txEUvgdDTAzHv2P24donTt6/529l+9Ua0vFImLlb"
  result=`get_integrity`
  [ "$result" == "$expected" ]
}
