# Making Armbian Live Distro Tutorial

2017-2018 Ivan Dedinski

## Introduction

Linux live distros are designed to be able to start a whole operating system
from a read-only media, but to behave as if the media was read-write.

The example - Debian Live. It can be burned on a CD and you can boot a full
featured debian system from it, even without a hard drive. You can edit and
save files, the system is writing logs, etc. After a reboot, everything is
lost, since all changes are "copyonwrited" in RAM.

A live distro has some nice properties for an ARM board running Linux:

- The media (sd card or emmc flash) can not be broken by write operations
- The file system can not be messed up by a misbehaving application
- The system can always reboot into a working state
- The boot is faster, since all writes are in RAM
- The media image size is small, since the file system is strongly
  compressed.

Write operations however use RAM. So, if you write too much, the RAM may be
exhausted. In most cacses this is not a problem though.


## Debian Live overview

A debian live distribution basically has the following components:

- a linux kernel with support for squashfs and overlayfs (aufs)
- a filesystem.squashfs compressed filesystem image containing all files in
  /. May be created by debootstrap, or from an existing non-live system,
  e.g. the available Armbian images.
- an initrd image which mounts the squashed filesystem via overlayfs, that
  makes the read-only squashfs writable, all writes are done in RAM, no
  changes are made to the squashfs.
- a bootloader, which passes the necessary parameters to the kernel and
  initrd


## Armbian live distro for BPI M2+


### Installing packages and modifying bootloader

As a starting point, I compiled a standard Armbian os image:
`Armbian_5.27_Bananapim2plus_Debian_jessie_default_3.4.113.img`.

The process is well documented, so I will spare the compilation details here. I
used the vendor kernel 3.4 and the only customization was to switch on the zram
staging driver. Zram can be used as a compressed RAM swap device, to increase
RAM capacity, but is not vital for the process. So you can skip zram, skip
Armbian compilation and start with an image which is already available. You
have to write the image on an SD-card.

The first thing to do is to boot the SD-card on the BPI M2+, setup root
password and users and then install live-boot:

apt-get update
apt-get install live-boot

The live-boot package modifies and updates the initrd image. 

After installing live-boot, you need to modify the bootloader a bit. Change the
bootargs line to this:

```
setenv bootargs "boot=live live-boot-path=/live union=overlayfs ${consoleargs} hdmi.audio=EDID:0 disp.screen0_output_mode=${disp_mode} consoleblank=0 loglevel=${verbosity} ubootpart=${partuuid} ubootsource=${devtype} ${extraargs} ${extraboardargs}"
```

You have to recompile the bootloader config with:

```bash
mkimage -C none -A arm -T script -d /boot/boot.cmd /boot/boot.scr
```

The modified bootargs tells to the initrd that it has to start live.
live-boot-path tells where to find the filesystem.squashfs. union=overlayfs
tells the initrd to use overlayfs instead of aufs (which was default for
jessie).

Aufs has been abandoned in newer systems, and it is not present in kernel 3.4.
Overlayfs however does the same job and is present.


### Kernel before 4.0: Fixing initrd

These instructions are for the older release with 3.14 kernel. In latest
releases (4.x kernel) the initrd problems are fixed and you only need to modify
the boot.cmd. Don't forget mkimage.

To begin, you have to shutdown the BPI and mount the SD-card partition
mmcblk0p1 on your PC somewhere, eg. /imagemnt

First you need to fix a small bug in the initrd created by live-boot in the
jessie distribution. It happens when you try to use overlayfs. In order to fix
it, you need to uncompress the initrd from the sdcard, modify a file, compress
it again.

That is how it is done:

```
tail -c+65 < /imagemnt/boot/uInitrd-3.4.113-sun8i > /tmp/out.gz
cd /tmp
mkdir initrd
cd initrd
gunzip /tmp/out.gz
cpio -i < /tmp/out
nano scripts/init-bottom/udev
```

Change 'mount -n -o move /dev ${rootmnt}/dev' to 'mount -n --move /dev
${rootmnt}/dev'. This means adding two dashes (`--`) in front of the `move`
keyword.

```
find . -print0 | cpio --null -ov --format=newc \
    | gzip -9 > /tmp/initramfs.cpio.gz
mkimage -A arm -T ramdisk -C none -n uInitrd -d /tmp/initramfs.cpio.gz \
    /imagemnt/boot/uInitrd-3.4.113-sun8i
```


### Preparing the squashfs image and the root file system

Now we are ready to make the filesystem.squashfs file. You need the
squashfs-utils for this:

```
mksquashfs /imagemnt /tmp/filesystem.squashfs
mkdir /imagemnt/live
mv /tmp/filesystem.squashfs /imagemnt/live
```

Now, since the filesystem is already in the squashfs, we can remove everything
from the image except /boot and /live:

```
cd /imagemnt
rm -rf bin  dev  etc  home  lib  media  mnt  opt  proc \
       root  run  sbin  selinux  srv  sys  tmp  usr  var
umount /imagemnt
```

The image or SD card is now ready to boot live. You may shrink the "mmcblk0p1"
partition, since the squashed filesystem occupies only a fraction of the
original size. 

### Overlayfs

In case an error appears with a message like "OverlayFS missing", you might
need to run `apt install overlayroot` on the device prior to saving the image
of the card.


## Notes

- you can also manipulate the image without writing it to an SD-card and
  booting it. You can use losetup and kpartx to mount the mmcblk0p1
  partition. You can chroot into it afterwards. But since this is arm
  distro, you need to copy qemu-arm-static into /usr/bin of the image
  first, see debian howto on cross platform chrooting.
- you should remove log2ram from the live distro, logs are now written to
  ram anyway
- fstab may be (has to be) empty, initrd mounts everything. You have to
  remove the swap and tmpfs entires from the original Armbian image. If you
  need some extra mounts (e.g. additional usb disk, etc.), you may add them
  of course.
- you may write the whole image to the emmc flash, the device will boot
  live the same way.
- if you use zram, it has to be compiled into the kernel, selecting it as a
  module won't work.
- zram can approximately double your RAM capacity
- you can unsquash the filesystem.squashfs file, chroot into it, make
  changes, squash it again and copy it in the /live folder. This is the
  only modification you need to change your system.
- if you want to change the kernel image, you have to overwrite the vmlinuz
  file in the /boot folder, and eventually you have to update
  /lib/modules/xxx inside the filesystem.squashfs image

## Compatibility

This recipe works fine for Debian-based Armbian. It has not been confirmed that it works on Ubuntu Bionic Armbian.

## Compatibility

This recipe works fine for Debian-based Armbian. It has not been confirmed that it works on Ubuntu Bionic Armbian.

## Authors

The original document was written by Ivan Dedinski. It was shared via [Google
Drive](https://drive.google.com/file/d/0B9rkZNVvi3bIaks1N2pQWEh3S1k/view) to
accompany a discussion on Armbian Forum. The discussion is located [here](
https://forum.armbian.com/index.php?/topic/4176-debian-live-boot-with-armbian/).
It was successively mirrored by user probonopd on [GitHub.com](
https://gist.github.com/probonopd/cf4993ca4ab5bd7757a525f19152a123),
where the original author later added his update in the form of a comment. The
pseudonym of the author on GitHub is iivanich.

