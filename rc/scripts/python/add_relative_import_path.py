#!/usr/bin/env python3

def add_relative_import_path(import_dir):

    from os.path import dirname, join
    import sys

    new_path = join(dirname(__file__), import_dir)
    sys.path.append(new_path)
