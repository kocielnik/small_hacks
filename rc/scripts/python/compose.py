#!/usr/bin/env python3

import functools

def compose2(f, g):

    """
    Credit: Mathieu Larose,
    Source: https://mathieularose.com/function-composition-in-python/

    >>> def double(x):
    ...     return x * 2
    >>> def inc(x):
    ...     return x + 1
    >>> def dec(x):
    ...     return x - 1
    >>> inc_double_and_dec = compose2(compose2(dec, double), inc)
    >>> inc_double_and_dec(10)
    21
    """

    return lambda x: f(g(x))

def compose(*functions):

    """
    Credit: Mathieu Larose,
    Source: https://mathieularose.com/function-composition-in-python/

    >>> def double(x):
    ...     return x * 2
    >>> def inc(x):
    ...     return x + 1
    >>> def dec(x):
    ...     return x - 1
    >>> inc_double_and_dec = compose(dec, double, inc)
    >>> inc_double_and_dec(10)
    21
    """

    return functools.reduce(compose2, functions, lambda x: x)

if __name__ == '__main__':

    import doctest

    doctest.testmod()

