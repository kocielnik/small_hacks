#!/usr/bin/env pytest

from sys import path
from add_relative_import_path import add_relative_import_path

def test_add_relative_import_path():
    add_relative_import_path("pretty_unique")
    match = filter(lambda x: "pretty_unique" in x, path)
    assert len(list(match)) > 0
