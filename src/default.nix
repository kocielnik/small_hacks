{ nixpkgs ? import <nixpkgs> {  } }:

let
  pkgs = [
    nixpkgs.gnumake
    nixpkgs.rstudio
    nixpkgs.rPackages.bookdown
  ];
in
  nixpkgs.stdenv.mkDerivation {
    name = "env";
    buildInputs = pkgs;
  }
