let
  pkgs = import <nixpkgs> {};
  stdenv = pkgs.stdenv;
in with pkgs; {
  myProject = stdenv.mkDerivation {
    name = "Speedracer";
    version = "1";

    buildInputs =  [
      R
      rPackages.bookdown
      rPackages.tufte
      rPackages.knitr
      rPackages.rmarkdown
      pkgs.texlive.combined.scheme-full
      pandoc
    ];
  };
}

# To compile the output documents, run:
#
# $ nix-shell --pure --run make
